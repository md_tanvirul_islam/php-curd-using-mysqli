<html>
<head>
    <title>Delete</title>
    <link href="css/index_style.css"  type="text/css" rel="stylesheet">
    <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <script src="js/jquery-3.5.1.slim.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>

</head>
<body>
<div class="container">
    <div class="div-com" id="div1">
        <h1>CRUD</h1>
    </div>

    <div class="div-com"  id="div2">
        <a href="index.php">Home</a>
        <a href="add.php">ADD</a>
        <a href="editbymail.php">UPDATE</a>
        <a href="delete.php">DELETE</a>
    </div>

    <div class="div-com" id="div3">
        <h4>Delete Record by Email</h4>

            <form action="<?=$_SERVER['PHP_SELF']?>" method="post">
                <div style="text-align: center">
                    <label for="email">Enter Your Registered Email</label>
                    <input type="email" class="form-control" name="email" id="email" style="width: 50%;margin:
                    auto;"><br>
                    <input type="submit" value="Search" name="subbtn" class="btn btn-info">
                </div>

            </form>



        <?php
            if(isset($_POST['subbtn']))
            {
                $email = $_POST["email"];
                include('config.php');
                $query="select * from users where email='{$email}'";
                $result = mysqli_query($con,$query) or die("Query is not Successful");
                if(mysqli_num_rows($result)>0)
                {
                    while ($data= mysqli_fetch_assoc($result))
                    {
                       // print_r($data);


        ?>

        <form action="delete_query.php" method="post">
        <table   style="width:60%;margin-left: auto;margin-right: auto;" class="table table-bordered">


            <tr>
                <td style="text-align: center;">Name :</td>
                <td style="text-align: center;"><?php echo $data['full_name'] ?></td>
            </tr>

            <tr>
                <td style="text-align: center">Birth Date:</td>
                <td style="text-align: center"> <?php echo $data['birth_date']?></td>
            </tr>

            <tr>
                <td style="text-align: center">Photo:</td>
                <td style="text-align: center"><img src="#" alt="<?php echo $data['full_name'].' Photo'?>"></td>
            </tr>
            <tr>
                <td style="text-align: center" >Email:</td>
                <td style="text-align: center" ><input type="email" value="<?=$data['email']?>" disabled ></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <input type="submit" value="Delete" class="btn btn-danger">

                </td>
            </tr>




         </table>
        </form>

    </div>


</div>
</body>
</html>

        <?php
        }
        }
        }
        ?>