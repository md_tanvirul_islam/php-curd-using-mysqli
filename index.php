<html>
    <head>
        <title>Home</title>
        <link href="css/index_style.css"  type="text/css" rel="stylesheet">
        <script src="js/jquery-3.5.1.slim.js" type="text/javascript"></script>
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <script src="js/bootstrap.min.js" type="text/javascript"></script>

    </head>
    <body>
        <div class="container">
            <div class="div-com" id="div1">
                    <h1>CRUD</h1>
            </div>

            <div class="div-com"  id="div2">
                    <a href="index.php">Home</a>
                    <a href="add.php">ADD</a>
                    <a href="editbymail.php">UPDATE</a>
                    <a href="delete.php">DELETE</a>
            </div>

            <div class="div-com" id="div3">
                <h4>All Records</h4>

                <?php
                    $con = mysqli_connect("localhost","root","","php_crud") or die("connection fail");
                    $sql= "SELECT id,full_name,email FROM users";

                    $result = mysqli_query($con,$sql) or die("Query Unsuccessful.");



                ?>

                <table style="width: 90%"  align="center" border="2px"   >
                    <tr>
                        <th style="width: 5%;text-align:center;">ID</th>
                        <th style="width: 30%;text-align:center;">Name</th>
                        <th style="width: 30%;text-align:center;">Email</th>
                        <th style="width: 40%;text-align:center;">Action</th>
                    </tr>

                    <?php
                    if(mysqli_num_rows($result)>0)
                    {
                        $row = mysqli_fetch_all($result,MYSQLI_ASSOC);



                        $count =0;
                         foreach($row as $data)
                            {


                    ?>
                    <tr>
                        <td style="text-align:center;"><?php echo ++$count;?></td>
                        <td style="text-align:center;"><?php echo $data["full_name"]; ?></td>
                        <td style="text-align:center;"><?php echo $data["email"]; ?></td>

                        <td style="text-align:center;padding-top: 3px;padding-bottom: 3px;">
                            <a class=" btn btn-outline-warning" href='edit.php?id=<?=$data["id"]?>'> EDIT</a>
                            <a class="btn btn-outline-info" href="profile.php">Details</a>
                            <a class="btn btn-outline-danger" href="delete_query.php?id=<?=$data['id']?>">DELETE</a>
                        </td>
                    </tr>
                    <?php } ?>

                </table>

                <?php
                    }
                 else
                    { echo"No Records Found";}
                mysqli_close($con);
                    ?>

            </div>


        </div>
    </body>
</html>
