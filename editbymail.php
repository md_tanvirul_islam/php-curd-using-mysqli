<html>
<head>
    <title>Update</title>
    <link href="css/index_style.css"  type="text/css" rel="stylesheet">
    <script src="js/jquery-3.5.1.slim.js" type="text/javascript"></script>
    <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <script src="js/bootstrap.min.js" type="text/javascript"></script>

</head>
<body>
<div class="container">
    <div class="div-com" id="div1">
        <h1>CRUD</h1>
    </div>

    <div class="div-com"  id="div2">
        <a href="index.php">Home</a>
        <a href="add.php">ADD</a>
        <a href="editbymail.php">UPDATE</a>
        <a href="delete.php">DELETE</a>
    </div>

    <div class="div-com" id="div3">
        <h4 >Change Your Record by Your Email </h4>
        <form action="<?= $_SERVER["PHP_SELF"];?>" method="post">
            <div style="text-align: center">
                <label for="uemail1">Enter Your Registered Email</label>
                <input type="email" class="form-control" name="uemail1" id="uemail1" style="width: 50%;margin: auto;"><br>
                <input type="submit" value="Search" name="subbtn" class="btn btn-info">
            </div>
        </form>


        <?php
            if(isset($_POST['subbtn']))
            {
                $email = $_POST["uemail1"];
                $con =mysqli_connect("localhost","root","","php_crud") or die("Connection Fail");
                $query="select * from users where email='{$email}'";
                $result = mysqli_query($con,$query) or die("Query is not Successful");
                if(mysqli_num_rows($result)>0)
                {
                    while ($data= mysqli_fetch_assoc($result))
                    {
                       // print_r($data);


        ?>

        <table style="width: 90%;margin-bottom:20px;"  align="center" border="2px"   >
            <form method="post" action="edit_query.php">
                <label for="uid" hidden>ID</label>
                <input type="hidden" name="uid" id="uid" value='<?=$data["id"] ?>'>
                <tr style="width: auto">
                    <td> Enter Your Full Name:</td>
                    <td> <input class="form-control" type="text" name="uname" value='<?=$data["full_name"] ?>'   required> </td>
                </tr>
                <tr>
                    <td> Enter Your Email:</td>
                    <td> <input class="form-control" type="email" name="uemail" value='<?=$data["email"] ?>'  required> </td>
                </tr>
                <tr>
                    <td> Enter Your Password:</td>
                    <td> <input class="form-control" type="password" name="upassword" value='<?=$data["password"] ?>'  required> </td>
                </tr>
                <tr>
                    <td> Enter Your Birth Date:</td>
                    <td> <input class="form-control" type="date" name="ubirthdate" value='<?=$data["birth_date"] ?>'  required> </td>
                </tr>
                <tr>
                    <td> Enter Your Phone:</td>
                    <td> <input class="form-control" type="text" name="uphone" value='<?=$data["phone"] ?>' required> </td>
                </tr>
                <tr>
                    <td> Enter Your Address:</td>
                    <td> <input class="form-control" type="text" name="uaddress"  value='<?=$data["address"] ?>' required> </td>
                </tr>
                <tr>
                    <td> Education Institute Name:</td>
                    <td> <input class="form-control" type="text" name="uedu_insti" value='<?=$data["edu_institute"] ?>'  required> </td>
                </tr>

                <tr>
                    <td> Class:</td>
                    <td> <input class="form-control" type="text" name="uclass" value='<?=$data["class"] ?>'  required> </td>
                </tr>


                <tr>
                    <td> Current Major or Department:</td>
                    <td> <input class="form-control" type="text" name="usubject" value='<?=$data["subject"] ?>'  required> </td>
                </tr>

                <tr>
                    <td> Upload Your Photo:</td>
                    <td> <input class="form-control" type="file" name="uphoto" value='<?=$data["photo"] ?>'  required> </td>
                </tr>
                <tr>
                    <td>
                        Tell About Yourself:
                    </td>
                    <td>
                        <textarea name="ubio" cols="68" rows="10"><?=$data["bio"] ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input class="btn btn-primary" type="submit" value="Update Your Info">
                    </td>
                </tr>


            </form>
        </table>
        <?php
                    }
                }
                unset($_POST);
            }

         ?>

    </div>


</div>
</body>
</html>

<?php
?>